import pyvisa as visa
import time
import xlsxwriter
import os
import shutil
# start of Untitled

#R2 - 20220503 - Updated to Python 3.9 - Ben

rm = visa.ResourceManager()
E5071B = rm.open_resource('TCPIP0::10.0.10.177::inst0::INSTR')
#E5071B = rm.open_resource('GPIB1::17::INSTR')


tester = input("Tested by:")
file_name = input("File name:")
f = open(file_name+'.txt','w')



workbook = xlsxwriter.Workbook(file_name+'.xlsx')
worksheet = workbook.add_worksheet()

worksheet.write(0, 0, 'Job Number:')
worksheet.write(1, 0, 'Tested by:')
worksheet.write(0, 1, str(file_name))
worksheet.write(1, 1, str(tester))
delay = 0.75

##E5071B.write(':DISPlay:FSIGn ON')
##E5071B.write(':SYSTem:BEEPer:WARNing:STATe ON')
E5071B.write(':DISPlay:WINDow1:ACTivate')
time.sleep(delay)
chan1_freq = E5071B.query_ascii_values(':SENSe1:FREQuency:DATA?')
E5071B.write(':DISPlay:WINDow2:ACTivate')
time.sleep(delay)
chan2_freq = E5071B.query_ascii_values(':SENSe2:FREQuency:DATA?')
E5071B.write(':DISPlay:WINDow3:ACTivate')
time.sleep(delay)
chan3_freq = E5071B.query_ascii_values(':SENSe3:FREQuency:DATA?')
##E5071B.write(':DISPlay:WINDow4:ACTivate')
##time.sleep(delay)
##chan4_freq = E5071B.query_ascii_values(':SENSe4:FREQuency:DATA?')

worksheet.write(2, 0, 'Frequency')
row = 3
column = 0
i = 0

while i < len(chan1_freq):
    worksheet.write(row, column, str(chan1_freq[i]))
    row = row + 1
    i = i + 1

worksheet.write(2, 5, 'Frequency')
row = 3
column = 5
i = 0
while i < len(chan2_freq):
    worksheet.write(row, column, str(chan2_freq[i]))
    row = row + 1
    i = i + 1

worksheet.write(2, 8, 'Frequency')
row = 3
column = 8
i = 0
while i < len(chan3_freq):
    worksheet.write(row, column, str(chan3_freq[i]))
    row = row + 1
    i = i + 1

worksheet.write(2, 14, 'Frequency')
row = 3
column = 14
i = 0
##while i < len(chan4_freq):
##    worksheet.write(row, column, str(chan4_freq[i]))
##    row = row + 1
##    i = i + 1

interested_start_frequency1_1 = 9000000
interested_stop_frequency1_1 = 100000000
i = 0
cond_start = 1
cond_stop = 1
while i < len(chan1_freq):
    if ((chan1_freq[i] >= interested_start_frequency1_1) and (cond_start == 1)):
        index_interested_start_frequency1_1 = i
        cond_start = 0
    if ((chan1_freq[i] >= interested_stop_frequency1_1) and (cond_stop == 1)):
        index_interested_stop_frequency1_1 = i - 1
        cond_stop = 0
    i = i + 1


interested_start_frequency2_1 = 9000000
interested_stop_frequency2_1 = 100000000
i = 0
cond_start = 1
cond_stop = 1
while i < len(chan2_freq):
    if ((chan2_freq[i] >= interested_start_frequency2_1) and (cond_start == 1)):
        index_interested_start_frequency2_1 = i
        cond_start = 0
    if ((chan2_freq[i] >= interested_stop_frequency2_1) and (cond_stop == 1)):
        index_interested_stop_frequency2_1 = i - 1
        cond_stop = 0
    i = i + 1


interested_start_frequency2_2 = 9000000
interested_stop_frequency2_2 = 100000000
i = 0
cond_start = 1
cond_stop = 1
while i < len(chan2_freq):
    if ((chan2_freq[i] >= interested_start_frequency2_2) and (cond_start == 1)):
        index_interested_start_frequency2_2 = i
        cond_start = 0
    if ((chan2_freq[i] >= interested_stop_frequency2_2) and (cond_stop == 1)):
        index_interested_stop_frequency2_2 = i - 1
        cond_stop = 0
    i = i + 1


interested_start_frequency2_3 = 9000000
interested_stop_frequency2_3 = 100000000
i = 0
cond_start = 1
cond_stop = 1
while i < len(chan2_freq):
    if ((chan2_freq[i] >= interested_start_frequency2_3) and (cond_start == 1)):
        index_interested_start_frequency2_3 = i
        cond_start = 0
    if ((chan2_freq[i] >= interested_stop_frequency2_3) and (cond_stop == 1)):
        index_interested_stop_frequency2_3 = i - 1
        cond_stop = 0
    i = i + 1


interested_start_frequency2_4 = 9000000
interested_stop_frequency2_4 = 100000000
i = 0
cond_start = 1
cond_stop = 1
while i < len(chan2_freq):
    if ((chan2_freq[i] >= interested_start_frequency2_4) and (cond_start == 1)):
        index_interested_start_frequency2_4 = i
        cond_start = 0
    if ((chan2_freq[i] >= interested_stop_frequency2_4) and (cond_stop == 1)):
        index_interested_stop_frequency2_4 = i - 1
        cond_stop = 0
    i = i + 1


interested_start_frequency4_1 = 9000000
interested_stop_frequency4_1 = 100000000
i = 0
cond_start = 1
cond_stop = 1
##while i < len(chan4_freq):
##    if ((chan4_freq[i] >= interested_start_frequency4_1) and (cond_start == 1)):
##        index_interested_start_frequency4_1 = i
##        cond_start = 0
##    if ((chan4_freq[i] >= interested_stop_frequency4_1) and (cond_stop == 1)):
##        index_interested_stop_frequency4_1 = i - 1
##        cond_stop = 0
##    i = i + 1


interested_start_frequency4_2 = 9000000
interested_stop_frequency4_2 = 100000000
i = 0
cond_start = 1
cond_stop = 1
##while i < len(chan4_freq):
##    if ((chan4_freq[i] >= interested_start_frequency4_2) and (cond_start == 1)):
##        index_interested_start_frequency4_2 = i
##        cond_start = 0
##    if ((chan4_freq[i] >= interested_stop_frequency4_2) and (cond_stop == 1)):
##        index_interested_stop_frequency4_2 = i - 1
##        cond_stop = 0
##    i = i + 1

#################
E5071B.write(':DISPlay:WINDow1:ACTivate')
time.sleep(delay)
temp_values = E5071B.query_ascii_values(':CALCulate1:TRACe1:DATA:FDATa?')

worksheet.write(2, 1, 'S11')
row = 3
column = 1
i = 0
while i < len(temp_values):
    if ((i % 2) == 0):
        worksheet.write(row, column, str(temp_values[i]))
        row = row + 1
    i = i + 1

index_interested_start_frequency1_1 = index_interested_start_frequency1_1 * 2
index_interested_stop_frequency1_1 = index_interested_stop_frequency1_1 * 2

i = index_interested_start_frequency1_1
input_return_loss = []
while i < index_interested_stop_frequency1_1 + 1:
    if ((i % 2) == 0):
        input_return_loss.append(temp_values[i])

    i = i + 1

##min_input_return_loss = min(input_return_loss)
##max_input_return_loss = max(input_return_loss)

##print "Min Input Return Loss (S11)"
##f.write('Min Input Return Loss (S11) \n')
##print min_input_return_loss
##f.write(str(min_input_return_loss) + '\n')
##print "Max Input Return Loss (S11)"
##f.write('Max Input Return Loss (S11) \n')
##print "Input Return Loss (S11)"
##f.write('Input Return Loss (S11) \n')
####print max_input_return_loss
##f.write(str(max_input_return_loss) + '\n')
#################

E5071B.write(':DISPlay:WINDow1:ACTivate')
time.sleep(delay)
temp_values = E5071B.query_ascii_values(':CALCulate1:TRACe2:DATA:FDATa?')

worksheet.write(2, 2, 'S22')
row = 3
column = 2
i = 0
while i < len(temp_values):
    if ((i % 2) == 0):
        worksheet.write(row, column, str(temp_values[i]))
        row = row + 1
    i = i + 1

i = index_interested_start_frequency1_1
output_return_loss = []
while i < index_interested_stop_frequency1_1 + 1:
    if ((i % 2) == 0):
        output_return_loss.append(temp_values[i])

    i = i + 1

##min_output_return_loss = min(output_return_loss)
##max_output_return_loss = max(output_return_loss)

##print "Min Output Return Loss (S22)"
##f.write('Min Output Return Loss (S22) \n')
##print min_output_return_loss
##f.write(str(min_output_return_loss) + '\n')
##print "Max Output Return Loss (S22)"
##f.write('Max Output Return Loss (S22) \n')
##print "Output Return Loss (S22)"
##f.write('Output Return Loss (S22) \n')
##print max_output_return_loss
##f.write(str(max_output_return_loss) + '\n')


#################

E5071B.write(':DISPlay:WINDow1:ACTivate')
time.sleep(delay)
temp_values = E5071B.query_ascii_values(':CALCulate1:TRACe3:DATA:FDATa?')

worksheet.write(2, 3, 'S44')
row = 3
column = 3
i = 0
while i < len(temp_values):
    if ((i % 2) == 0):
        worksheet.write(row, column, str(temp_values[i]))
        row = row + 1
    i = i + 1

#################
E5071B.write(':DISPlay:WINDow2:ACTivate')
time.sleep(delay)
temp_values = E5071B.query_ascii_values(':CALCulate2:TRACe1:DATA:FDATa?')

worksheet.write(2, 6, 'S21')
row = 3
column = 6
i = 0
while i < len(temp_values):
    if ((i % 2) == 0):
        worksheet.write(row, column, str(temp_values[i]))
        row = row + 1
    i = i + 1

index_interested_start_frequency2_1 = index_interested_start_frequency2_1 * 2
index_interested_stop_frequency2_1 = index_interested_stop_frequency2_1 * 2

i = index_interested_start_frequency2_1
thru_loss_950_2100 = []
while i < index_interested_stop_frequency2_1 + 1:
    if ((i % 2) == 0):
        thru_loss_950_2100.append(temp_values[i])

    i = i + 1

##min_thru_loss_950_2100 = min(thru_loss_950_2100)
##max_thru_loss_950_2100 = max(thru_loss_950_2100)

##print "Min thru loss between 950MHz and 2100MHz"
##f.write('Min thru loss between 950MHz and 2100MHz \n')
##print "thru loss between 950MHz and 2100MHz"
##f.write('thru loss between 950MHz and 2100MHz \n')
##print min_thru_loss_950_2100
##f.write(str(min_thru_loss_950_2100) + '\n')
##print "Max thru loss between 950MHz and 2100MHz"
##f.write('Max thru loss between 950MHz and 2100MHz \n')
##print max_thru_loss_950_2100
##f.write(str(max_thru_loss_950_2100) + '\n')

#################
E5071B.write(':DISPlay:WINDow3:ACTivate')
time.sleep(delay)
temp_values = E5071B.query_ascii_values(':CALCulate3:TRACe1:DATA:FDATa?')

worksheet.write(2, 9, 'S33')
row = 3
column = 9
i = 0
while i < len(temp_values):
    if ((i % 2) == 0):
        worksheet.write(row, column, str(temp_values[i]))
        row = row + 1
    i = i + 1

E5071B.write(':DISPlay:WINDow3:ACTivate')
time.sleep(delay)
temp_values = E5071B.query_ascii_values(':CALCulate3:TRACe2:DATA:FDATa?')

worksheet.write(2, 10, 'S32')
row = 3
column = 10
i = 0
while i < len(temp_values):
    if ((i % 2) == 0):
        worksheet.write(row, column, str(temp_values[i]))
        row = row + 1
    i = i + 1

E5071B.write(':DISPlay:WINDow3:ACTivate')
time.sleep(delay)
temp_values = E5071B.query_ascii_values(':CALCulate3:TRACe3:DATA:FDATa?')

worksheet.write(2, 11, 'S24')
row = 3
column = 11
i = 0
while i < len(temp_values):
    if ((i % 2) == 0):
        worksheet.write(row, column, str(temp_values[i]))
        row = row + 1
    i = i + 1

E5071B.write(':DISPlay:WINDow3:ACTivate')
time.sleep(delay)
temp_values = E5071B.query_ascii_values(':CALCulate3:TRACe4:DATA:FDATa?')

worksheet.write(2, 12, 'S34')
row = 3
column = 12
i = 0
while i < len(temp_values):
    if ((i % 2) == 0):
        worksheet.write(row, column, str(temp_values[i]))
        row = row + 1
    i = i + 1


#################


##E5071B.write(':DISPlay:WINDow2:ACTivate')
##temp_values = E5071B.query_ascii_values(':CALCulate2:TRACe1:DATA:FDATa?')
##
##index_interested_start_frequency2_2 = index_interested_start_frequency2_2 * 2
##index_interested_stop_frequency2_2 = index_interested_stop_frequency2_2 * 2
##
##i = index_interested_start_frequency2_2
##isolation_10MHz = []
##while i < index_interested_stop_frequency2_2 + 1:
##    if ((i % 2) == 0):
##        isolation_10MHz.append(temp_values[i])
##
##    i = i + 1
##
##min_isolation_10MHz = min(isolation_10MHz)
##max_isolation_10MHz = max(isolation_10MHz)
##
####print "Min 10MHz isolation"
####f.write('Min 10MHz isolation \n')
####print min_isolation_10MHz
####f.write(str(min_isolation_10MHz) + '\n')
####print "Max 10MHz isolation"
####f.write('Max 10MHz isolation \n')
##print "10MHz isolation"
##f.write('10MHz isolation \n')
##print max_isolation_10MHz
##f.write(str(max_isolation_10MHz) + '\n')
#################


#################
E5071B.write(':DISPlay:WINDow2:ACTivate')
temp_values = E5071B.query_ascii_values(':CALCulate2:TRACe1:DATA:FDATa?')

index_interested_start_frequency2_3 = index_interested_start_frequency2_3 * 2
index_interested_stop_frequency2_3 = index_interested_stop_frequency2_3 * 2

i = index_interested_start_frequency2_3
thru_loss_2100_3400 = []
while i < index_interested_stop_frequency2_3 + 1:
    if ((i % 2) == 0):
        thru_loss_2100_3400.append(temp_values[i])

    i = i + 1

##min_thru_loss_2100_3400 = min(thru_loss_2100_3400)
##max_thru_loss_2100_3400 = max(thru_loss_2100_3400)

##print "Min thru loss between 2100MHz and 3400MHz"
##f.write('Min thru loss between 2100MHz and 3400MHz \n')
##print "thru loss between 2100MHz and 3400MHz"
##f.write('thru loss between 2100MHz and 3400MHz \n')
##print min_thru_loss_2100_3400
##f.write(str(min_thru_loss_2100_3400) + '\n')
##print "Max thru loss between 2100MHz and 3400MHz"
##f.write('Max thru loss between 2100MHz and 3400MHz \n')
##print max_thru_loss_2100_3400
##f.write(str(max_thru_loss_2100_3400) + '\n')
#################


#################
E5071B.write(':DISPlay:WINDow2:ACTivate')
temp_values = E5071B.query_ascii_values(':CALCulate2:TRACe1:DATA:FDATa?')

index_interested_start_frequency2_4 = index_interested_start_frequency2_4 * 2
index_interested_stop_frequency2_4 = index_interested_stop_frequency2_4 * 2

i = index_interested_start_frequency2_4
thru_loss_3400_4000 = []
while i < index_interested_stop_frequency2_4 + 1:
    if ((i % 2) == 0):
        thru_loss_3400_4000.append(temp_values[i])

    i = i + 1

##min_thru_loss_3400_4000 = min(thru_loss_3400_4000)
##max_thru_loss_3400_4000 = max(thru_loss_3400_4000)

##print "Min thru loss between 3400MHz and 4000MHz"
##f.write('Min thru loss between 3400MHz and 4000MHz \n')
##print "thru loss between 3400MHz and 4000MHz"
##f.write('thru loss between 3400MHz and 4000MHz \n')
##print min_thru_loss_3400_4000
##f.write(str(min_thru_loss_3400_4000) + '\n')
##print "Max thru loss between 3400MHz and 4000MHz"
##f.write('Max thru loss between 3400MHz and 4000MHz \n')
##print max_thru_loss_3400_4000
##f.write(str(max_thru_loss_3400_4000) + '\n')
#################


#################
##E5071B.write(':DISPlay:WINDow4:ACTivate')
##time.sleep(delay)
##temp_values = E5071B.query_ascii_values(':CALCulate4:TRACe1:DATA:FDATa?')
##
##worksheet.write(2, 15, 'S14')
##row = 3
##column = 15
##i = 0
##while i < len(temp_values):
##    if ((i % 2) == 0):
##        worksheet.write(row, column, str(temp_values[i]))
##        row = row + 1
##    i = i + 1
##
##
##index_interested_start_frequency4_1 = index_interested_start_frequency4_1 * 2
##index_interested_stop_frequency4_1 = index_interested_stop_frequency4_1 * 2
##
##i = index_interested_start_frequency4_1
##thru_loss_10MHz = []
##while i < index_interested_stop_frequency4_1 + 1:
##    if ((i % 2) == 0):
##        thru_loss_10MHz.append(temp_values[i])
##
##    i = i + 1
##
##min_thru_loss_10MHz = min(thru_loss_10MHz)
##max_thru_loss_10MHz = max(thru_loss_10MHz)
##
####print "Min 10MHz thru loss"
####f.write('Min 10MHz thru loss \n')
##print "10MHz thru loss"
##f.write('10MHz thru loss \n')
##print min_thru_loss_10MHz
##f.write(str(min_thru_loss_10MHz) + '\n')
####print "Max 10MHz thru loss"
####f.write('Max 10MHz thru loss \n')
####print max_thru_loss_10MHz
####f.write(str(max_thru_loss_10MHz) + '\n')
###################
##
###################
##E5071B.write(':DISPlay:WINDow4:ACTivate')
##temp_values = E5071B.query_ascii_values(':CALCulate4:TRACe1:DATA:FDATa?')
##
##index_interested_start_frequency4_2 = index_interested_start_frequency4_2 * 2
##index_interested_stop_frequency4_2 = index_interested_stop_frequency4_2 * 2
##
##i = index_interested_start_frequency4_2
##thru_loss_11_100 = []
##while i < index_interested_stop_frequency4_2 + 1:
##    if ((i % 2) == 0):
##        thru_loss_11_100.append(temp_values[i])
##
##    i = i + 1
##
##min_thru_loss_11_100 = min(thru_loss_11_100)
##max_thru_loss_11_100 = max(thru_loss_11_100)
##
####print "Min thru loss between 11MHz and 100MHz"
####f.write('Min thru loss between 11MHz and 100MHz \n')
##print "thru loss between 11MHz and 100MHz"
##f.write('thru loss between 11MHz and 100MHz \n')
##print min_thru_loss_11_100
##f.write(str(min_thru_loss_11_100) + '\n')
####print "Max thru loss between 11MHz and 100MHz"
####f.write('Max thru loss between 11MHz and 100MHz \n')
####print max_thru_loss_11_100
####f.write(str(max_thru_loss_11_100) + '\n')
###################

ftemp = file_name + ".S4P"
filed = "P:\\" + file_name + ".S4P"
##Line to save S4P files
E5071B.write('MMEM:STOR:SNP:TYPE:S4P 1,2,3,4')
E5071B.write('MMEM:STOR:SNP "%s"' %filed)
time.sleep(1)
## Move file
path = os.path.dirname(__file__)
print(str(path))
shutil.move(filed, path)

#setting the format to save a screenshot
#E5071B.values_format.is_binary = True
#E5071B.values_format.datatype = 'B'
#E5071B.values_format.is_big_endian = False
#E5071B.values_format.container = bytearray

#saving a screenshot on the equipment
E5071B.write(':MMEMory:STORe:IMAGe "%s"' % ('D:Image01.png'))

#fetching the image data in an array
fetch_image_data = E5071B.query_binary_values(':MMEMory:TRANsfer? "D:Image01.png"',datatype='B',is_big_endian=False,container=bytearray)

#creating a file with the product number to write the image data to it
save_dir = open(str(file_name)+".PNG", 'wb')
save_dir.write(fetch_image_data)
save_dir.close()

E5071B.write(':SYSTem:BEEPer:WARNing:STATe OFF')
E5071B.write(':DISPlay:FSIGn OFF')

#deleting the image on the equipment
E5071B.write(':MMEMory:DELete "%s"' % ('D:Image01.png'))
E5071B.write("*CLS")







workbook.close()
f.close()
E5071B.close()
rm.close()

##


##i = index_interested_start_frequency
##interested_NF = []
##while i < index_interested_stop_frequency + 1:
##    interested_NF.append(NF[i])
##    i = i + 1
##min_NF = min(interested_NF)
##max_NF = max(interested_NF)
##print min_NF
##print max_NF
#################
##E5071B.write(':DISPlay:WINDow1:ACTivate')
##temp_values = E5071B.query_ascii_values(':CALCulate1:TRACe1:DATA:FDATa?')
##i = 0
##
##chan1_trace1 = []
##while i < len(temp_values):
##    if ((i % 2) == 0):
##        chan1_trace1.append(temp_values[i])
##
##    i = i + 1
###print chan3_trace1
##
##min_chan1_trace1 = min(chan1_trace1)
##max_chan1_trace1 = max(chan1_trace1)
##
##print "Min channel 1 trace 1"
##print min_chan1_trace1
##print "Max channel 1 trace 1"
##print max_chan1_trace1
#################
##
#################
##E5071B.write(':DISPlay:WINDow1:ACTivate')
##temp_values = E5071B.query_ascii_values(':CALCulate1:TRACe2:DATA:FDATa?')
##i = 0
##
##chan1_trace2 = []
##while i < len(temp_values):
##    if ((i % 2) == 0):
##        chan1_trace2.append(temp_values[i])
##
##    i = i + 1
###print chan3_trace1
##
##min_chan1_trace2 = min(chan1_trace2)
##max_chan1_trace2 = max(chan1_trace2)
##
##print "Min channel 1 trace 2"
##print min_chan1_trace2
##print "Max channel 1 trace 2"
##print max_chan1_trace2
#################
##
##
#################
##E5071B.write(':DISPlay:WINDow1:ACTivate')
##temp_values = E5071B.query_ascii_values(':CALCulate1:TRACe3:DATA:FDATa?')
##i = 0
##
##chan1_trace3 = []
##while i < len(temp_values):
##    if ((i % 2) == 0):
##        chan1_trace3.append(temp_values[i])
##
##    i = i + 1
###print chan3_trace1
##
##min_chan1_trace3 = min(chan1_trace3)
##max_chan1_trace3 = max(chan1_trace3)
##
##print "Min channel 1 trace 3"
##print min_chan1_trace3
##print "Max channel 1 trace 3"
##print max_chan1_trace3
#################
##
##
#################
##E5071B.write(':DISPlay:WINDow2:ACTivate')
##temp_values = E5071B.query_ascii_values(':CALCulate2:TRACe1:DATA:FDATa?')
##i = 0
##
##chan2_trace1 = []
##while i < len(temp_values):
##    if ((i % 2) == 0):
##        chan2_trace1.append(temp_values[i])
##
##    i = i + 1
###print chan3_trace1
##
##min_chan2_trace1 = min(chan2_trace1)
##max_chan2_trace1 = max(chan2_trace1)
##
##print "Min channel 2 trace 1"
##print min_chan2_trace1
##print "Max channel 2 trace 1"
##print max_chan2_trace1
#################
##
##
#################
##E5071B.write(':DISPlay:WINDow3:ACTivate')
##temp_values = E5071B.query_ascii_values(':CALCulate3:TRACe1:DATA:FDATa?')
##i = 0
##
##chan3_trace1 = []
##while i < len(temp_values):
##    if ((i % 2) == 0):
##        chan3_trace1.append(temp_values[i])
##
##    i = i + 1
###print chan3_trace1
##
##min_chan3_trace1 = min(chan3_trace1)
##max_chan3_trace1 = max(chan3_trace1)
##
##print "Min channel 3 trace 1"
##print min_chan3_trace1
##print "Max channel 3 trace 1"
##print max_chan3_trace1
#################
##
##
#################
##E5071B.write(':DISPlay:WINDow3:ACTivate')
##temp_values = E5071B.query_ascii_values(':CALCulate3:TRACe2:DATA:FDATa?')
##i = 0
##
##chan3_trace2 = []
##while i < len(temp_values):
##    if ((i % 2) == 0):
##        chan3_trace2.append(temp_values[i])
##
##    i = i + 1
###print chan3_trace1
##
##min_chan3_trace2 = min(chan3_trace2)
##max_chan3_trace2 = max(chan3_trace2)
##
##print "Min channel 3 trace 2"
##print min_chan3_trace2
##print "Max channel 3 trace 2"
##print max_chan3_trace2
#################
##
#################
##E5071B.write(':DISPlay:WINDow3:ACTivate')
##temp_values = E5071B.query_ascii_values(':CALCulate3:TRACe3:DATA:FDATa?')
##i = 0
##
##chan3_trace3 = []
##while i < len(temp_values):
##    if ((i % 2) == 0):
##        chan3_trace3.append(temp_values[i])
##
##    i = i + 1
###print chan3_trace1
##
##min_chan3_trace3 = min(chan3_trace3)
##max_chan3_trace3 = max(chan3_trace3)
##
##print "Min channel 3 trace 3"
##print min_chan3_trace3
##print "Max channel 3 trace 3"
##print max_chan3_trace3
#################
##
##
#################
##E5071B.write(':DISPlay:WINDow3:ACTivate')
##temp_values = E5071B.query_ascii_values(':CALCulate3:TRACe4:DATA:FDATa?')
##i = 0
##
##chan3_trace4 = []
##while i < len(temp_values):
##    if ((i % 2) == 0):
##        chan3_trace4.append(temp_values[i])
##
##    i = i + 1
###print chan3_trace1
##
##min_chan3_trace4 = min(chan3_trace4)
##max_chan3_trace4 = max(chan3_trace4)
##
##print "Min channel 3 trace 4"
##print min_chan3_trace4
##print "Max channel 3 trace 4"
##print max_chan3_trace4
#################
##
##
#################
##E5071B.write(':DISPlay:WINDow4:ACTivate')
##temp_values = E5071B.query_ascii_values(':CALCulate4:TRACe1:DATA:FDATa?')
##i = 0
##
##chan4_trace1 = []
##while i < len(temp_values):
##    if ((i % 2) == 0):
##        chan4_trace1.append(temp_values[i])
##
##    i = i + 1
###print chan3_trace1
##
##min_chan4_trace1 = min(chan4_trace1)
##max_chan4_trace1 = max(chan4_trace1)
##
##print "Min channel 4 trace 1"
##print min_chan4_trace1
##print "Max channel 4 trace 1"
##print max_chan4_trace1
#################




# end of Untitled

