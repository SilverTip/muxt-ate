#MuxT ATE R13 for E5071B MUXT Test bench
#written by Benjamin Stadnik
#Orbital Research Ltd.
#2021-02-24

#R14 - 20220503 - Uprgraded to Python 3 - Ben

import pyvisa as visa
import datetime
import time
import xlsxwriter
import os
import shutil

#global variables
delay = 0.75
rounding = 3

#Functions
#--------------------------------------------

def Write_to_Workbook(worksheet, name, start_row, column, values): #Input name and array of values and writes them to a worksheet
    worksheet.write(start_row, column, name)
    row = start_row + 1
    i = 0
    while i < len(values):
        worksheet.write(row, column, str(values[i]))
        row += 1
        i += 1

def Remove_Odd(values): #Data from E5071B gives data separated by zeros. This function is used to remove zeros 
    i = 0
    output = []
    while i < len(values):
        if i % 2 == 0:
              output.append(values[i])
        i += 1
    return output

def Find_Index(values, start_freq, stop_freq): #Using the frequency data, we index the ranges we want to isolate. 
    i = 0
    flag = 0
    while i < len(values):
        if ((values[i] >= start_freq) and (flag == 0)):
            index_start = i
            flag = 1
        if values[i] >= stop_freq:
            index_stop = i
            break
        i += 1
    index = [index_start, index_stop]
    return index

def Max_Value(values, index): #Find max value in between 2 indexes
    array = []
    i = index[0]
    while i <= index[1]:
        array.append(values[i])
        i += 1
    return max(array)

def Min_Value(values, index): #Find min value in between 2 indexes
    array = []
    i = index[0]
    while i <= index[1]:
        array.append(values[i])
        i += 1
    return min(array)

def timestamp(): #Get current time, format it, and return it
    now = datetime.datetime.now()
    now = now.strftime("%d %b %Y %H:%M:%S")
    return now

#Start of Main
#--------------------------------------------

#Connect with E5071B
rm = visa.ResourceManager()
E5071B = rm.open_resource('TCPIP0::10.0.10.188::inst0::INSTR')

#Get time
now = timestamp()

print(os.path.basename(__file__))
print(now + '\n')

#User input
tester = input("Tested by:")
file_name = input("Unit Serial Number:")
input("Press Enter to Begin")

#Setup .txt file
f = open(file_name + '.txt','w')
f.write('Serial Number: ' + file_name + '\n')
f.write('Tester: ' + tester + '\n')
f.write(os.path.basename(__file__) + '\n')
f.write(str(now) + '\n\n')

#Create workbook
workbook = xlsxwriter.Workbook(file_name +'.xlsx')
worksheet = workbook.add_worksheet()
worksheet.write(0, 0, 'S/N:')
worksheet.write(1, 0, 'Tested by:')
worksheet.write(0, 1, str(file_name))
worksheet.write(1, 1, str(tester))
worksheet.write(0, 3, 'Date:')
worksheet.write(0, 4, str(now))


#Aquire Data From Window #1
#-------------------------------------------------------------------------------------
#Enter interested freqeuncy range:
freq_start = 900000000
freq_stop = 2100000000
#--------------------------------
E5071B.write(':DISPlay:WINDow1:ACTivate')
freq = E5071B.query_ascii_values(':SENSe1:FREQuency:DATA?')
Unformatted_Trace1 = E5071B.query_ascii_values(':CALCulate1:TRACe1:DATA:FDATa?')
Unformatted_Trace2 = E5071B.query_ascii_values(':CALCulate1:TRACe2:DATA:FDATa?')
Unformatted_Trace3 = E5071B.query_ascii_values(':CALCulate1:TRACe3:DATA:FDATa?')
index = Find_Index(freq, freq_start, freq_stop)

#Format trace data
Trace1 = Remove_Odd(Unformatted_Trace1)
Trace2 = Remove_Odd(Unformatted_Trace2)
Trace3 = Remove_Odd(Unformatted_Trace3)

#Write trace data to workbook
Write_to_Workbook(worksheet, 'Frequency', 2, 0, freq)
Write_to_Workbook(worksheet, 'S11', 2, 1, Trace1)
Write_to_Workbook(worksheet, 'S22', 2, 2, Trace2)
Write_to_Workbook(worksheet, 'S44', 2, 3, Trace3)

#Find maximum value in each trace and round to 'rounding' decimal points
RL1 = round(Max_Value(Trace1, index), rounding)
RL2 = round(Max_Value(Trace2, index), rounding)
RL3 = round(Max_Value(Trace3, index), rounding)

#Write maximum value to file
f.write('Return Loss (S11):' + str(RL1) + ' dB \n')
f.write('Return Loss (S22):' + str(RL2) + ' dB \n')

#Write maximum value to console
print('Return Loss (S11):' + str(RL1) + ' dB')
print('Return Loss (S22):' + str(RL2) + ' dB')


#Aquire Data From Window #2
#-------------------------------------------------------------------------------------
#Enter interested freqeuncy ranges:
freq_start1 = 950000000
freq_stop1 = 2150000000

freq_start2 = 2150000000
freq_stop2 = 3000000000

freq_start3 = 3000000000
freq_stop3 = 4000000000
#--------------------------------
E5071B.write(':DISPlay:WINDow2:ACTivate')
freq = E5071B.query_ascii_values(':SENSe2:FREQuency:DATA?')
Unformatted_Trace1 = E5071B.query_ascii_values(':CALCulate2:TRACe1:DATA:FDATa?')
index1 = Find_Index(freq, freq_start1, freq_stop1)
index2 = Find_Index(freq, freq_start2, freq_stop2)
index3 = Find_Index(freq, freq_start3, freq_stop3)

#Format trace data
Trace1 = Remove_Odd(Unformatted_Trace1)

#Write trace data to workbook
Write_to_Workbook(worksheet, 'Frequency', 2, 5, freq)
Write_to_Workbook(worksheet, 'S21', 2, 6, Trace1)

#Find maximum value in each trace and round to 'rounding' decimal points
IL1 = round(Min_Value(Trace1, index1), rounding)
IL2 = round(Min_Value(Trace1, index2), rounding)
IL3 = round(Min_Value(Trace1, index3), rounding)

#Write maximum value to file
f.write('Insertion Loss (S21) from ' + str(freq_start1 / 1000000) + 'MHz to ' + str(freq_stop1 / 1000000) + 'MHz: ' + str(IL1) + ' dB \n')
f.write('Insertion Loss (S21) from ' + str(freq_start2 / 1000000) + 'MHz to ' + str(freq_stop2 / 1000000) + 'MHz: ' + str(IL2) + ' dB \n')
f.write('Insertion Loss (S21) from ' + str(freq_start3 / 1000000) + 'MHz to ' + str(freq_stop3 / 1000000) + 'MHz: ' + str(IL3) + ' dB \n')

#Write maximum value to console
print('Insertion Loss (S21) from ' + str(freq_start1 / 1000000) + 'MHz to ' + str(freq_stop1 / 1000000) + 'MHz: ' + str(IL1) + ' dB')
print('Insertion Loss (S21) from ' + str(freq_start2 / 1000000) + 'MHz to ' + str(freq_stop2 / 1000000) + 'MHz: ' + str(IL2) + ' dB')
print('Insertion Loss (S21) from ' + str(freq_start3 / 1000000) + 'MHz to ' + str(freq_stop3 / 1000000) + 'MHz: ' + str(IL3) + ' dB')
#----------------------------


#Aquire Data From Window #3
#-------------------------------------------------------------------------------------
#Enter interested freqeuncy ranges:
freq_start = 950000000
freq_stop = 2100000000
#--------------------------------
E5071B.write(':DISPlay:WINDow3:ACTivate')
freq = E5071B.query_ascii_values(':SENSe3:FREQuency:DATA?')
Unformatted_Trace1 = E5071B.query_ascii_values(':CALCulate3:TRACe1:DATA:FDATa?')
Unformatted_Trace2 = E5071B.query_ascii_values(':CALCulate3:TRACe2:DATA:FDATa?')
Unformatted_Trace3 = E5071B.query_ascii_values(':CALCulate3:TRACe3:DATA:FDATa?')
Unformatted_Trace4 = E5071B.query_ascii_values(':CALCulate3:TRACe4:DATA:FDATa?')
index = Find_Index(freq, freq_start, freq_stop)

#Format trace data
Trace1 = Remove_Odd(Unformatted_Trace1)
Trace2 = Remove_Odd(Unformatted_Trace2)
Trace3 = Remove_Odd(Unformatted_Trace3)
Trace4 = Remove_Odd(Unformatted_Trace4)

#Write trace data to workbook
Write_to_Workbook(worksheet, 'Frequency', 2, 8, freq)
Write_to_Workbook(worksheet, 'S34', 2, 9, Trace1)
Write_to_Workbook(worksheet, 'S32', 2, 10, Trace2)
Write_to_Workbook(worksheet, 'S24', 2, 11, Trace3)
Write_to_Workbook(worksheet, 'S33', 2, 12, Trace4)
#----------------------------


#Aquire Data From Window #4
#-------------------------------------------------------------------------------------
#Enter interested freqeuncy ranges:
freq_start1 = 9900000
freq_stop1 = 10100000

freq_start2 = 11000000
freq_stop2 = 100000000
#--------------------------------
E5071B.write(':DISPlay:WINDow4:ACTivate')
freq = E5071B.query_ascii_values(':SENSe4:FREQuency:DATA?')
Unformatted_Trace1 = E5071B.query_ascii_values(':CALCulate4:TRACe1:DATA:FDATa?')
index1 = Find_Index(freq, freq_start1, freq_stop1)
index2 = Find_Index(freq, freq_start2, freq_stop2)

#Format trace data
Trace1 = Remove_Odd(Unformatted_Trace1)

#Write trace data to workbook
Write_to_Workbook(worksheet, 'Frequency', 2, 14, freq)
Write_to_Workbook(worksheet, 'S14', 2, 15, Trace1)

#Find maximum value in each trace and round to 'rounding' decimal points
IL1 = round(Min_Value(Trace1, index1), rounding)
IL2 = round(Min_Value(Trace1, index2), rounding)

#Write maximum value to file
f.write('Insertion Loss (S14) at 10MHz: ' + str(IL1) + ' dB \n')
f.write('Insertion Loss (S14) from ' + str(freq_start2 / 1000000) + 'MHz to ' + str(freq_stop2 / 1000000) + 'MHz: ' + str(IL2) + ' dB \n')

#Write maximum value to console
print('Insertion Loss (S14) at 10MHz: ' + str(IL1) + ' dB')
print('Insertion Loss (S14) from ' + str(freq_start2 / 1000000) + 'MHz to ' + str(freq_stop2 / 1000000) + 'MHz: ' + str(IL2) + ' dB')

#----------------------------

def save_S4P(): 
    ##Line(s) to save S4P files
    filed = "P:\\"+ file_name + ".S4P"
    E5071B.write('MMEM:STOR:SNP:TYPE:S4P 1,2,3,4')
    E5071B.write('MMEM:STOR:SNP "%s"' %filed)
    time.sleep(1)

    ## Move S4P file to working directory
    path = os.path.dirname(__file__)
    print(str(path))
    shutil.move(filed, path)

def screenshot():
    #setting the format to save a screenshot
    E5071B.values_format.is_binary = True
    E5071B.values_format.datatype = 'B'
    E5071B.values_format.is_big_endian = False
    E5071B.values_format.container = bytearray

    #saving a screenshot on the equipment
    E5071B.write(':MMEMory:STORe:IMAGe "%s"' % ('D:Image01.png'))

    #fetching the image data in an array
    fetch_image_data = block = E5071B.query_values(':MMEMory:TRANsfer? "%s"' % ('D:Image01.png'))

    #creating a file with the product number to write the image data to it
    save_dir = open(str(file_name)+".PNG", 'wb')
    save_dir.write(fetch_image_data)
    save_dir.close()

    #deleting the image on the equipment
    E5071B.write(':MMEMory:DELete "%s"' % ('D:Image01.png'))
    E5071B.write("*CLS")

save_S4P()
screenshot()
workbook.close()
f.close()
E5071B.close()
rm.close()

