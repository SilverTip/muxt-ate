#N3300A Load Test Utility for MUXT Test bench
#written by Benjamin Stadnik
#Orbital Research Ltd.
#2021-02-25

#R2 - 20220503 - Uprageded to Python 3 -Ben

import pyvisa as visa
import datetime
import time
import xlsxwriter
import os

#Global variables
#------------------------------------------
sleep = 300 #Test duration in seconds
delay = 2 #seconds
rounding = 3 # Voltage level decimal places
current_limit = 5 #Current level upper bound. Safety check. 
#------------------------------------------

def timestamp(): #Get current time, format it, and return it
    now = datetime.datetime.now()
    now = now.strftime("%b %d %Y %H:%M")
    return now

def Create_Workbook(filename):
    global workbook, worksheet
    workbook = xlsxwriter.Workbook(filename +' Load Test.xlsx')
    worksheet = workbook.add_worksheet()
    
    #Add headers
    worksheet.write(0, 0, str(unit) + ' LOAD TEST')
    worksheet.write(1, 0, 'S/N:')
    worksheet.write(2, 0, 'Tested by:')
    worksheet.write(3, 0, 'Date:')
    worksheet.write(1, 1, str(filename))
    worksheet.write(2, 1, str(tester))
    worksheet.write(3, 1, str(starttime))

    worksheet.write(5, 0, 'Load Test Input:')
    worksheet.write(5, 1, '18V @ J3')
    worksheet.write(5, 2, str_current + 'A')
    worksheet.write(6, 0, 'Beginning')
    worksheet.write(7, 0, 'End')
    worksheet.write(8, 0, 'Duration')
    worksheet.write(8, 1, str(sleep) + ' seconds')

def find_number_in_string(string):
    number = float(filter(str.isdigit, string))
    return number

#Main
#--------------------------------------------------------------
#Connect with E5071B
rm = visa.ResourceManager()
#N3300A = rm.open_resource('GPIB1::6::INSTR')
N3300A = rm.open_resource('GPIB4::6::INSTR')

#Get time
starttime = timestamp()
print(os.path.basename(__file__))
print(starttime + '\n')

#User input
print('DTD or DET not yet supported.')
unit = input('Enter unit type: [MT25/TT25/etc.]')
temp = find_number_in_string(unit)
current = float(temp/10)
str_current = str(current)
print('Current: ' + str_current + 'A')
tester = input("Tested by:")
print('Load Test of ' + unit + '. 18V @ J3, ' + str_current + 'A @ J1')

#Safety fuction. Chack that the user hasn't inputted current level that could harm the device
if current >= current_limit:
    print ('ERROR: Current above 5A. Stopping program')
    exit()

#Set N3300A current level    
#N3300A.write('CURRent ' + str_current)
time.sleep(delay)
default = ''

while(1):
    filename = raw_input("Enter unit serial number:")
    raw_input("Press Enter to Begin")
    N3300A.write('CURRent ' + str_current)
    Create_Workbook(filename)

    unformatted_start_voltage = N3300A.query_ascii_values('MEASure:VOLTage?')
    start_voltage = str(round(float(unformatted_start_voltage[0]), rounding)) + 'V'
    worksheet.write(6, 1, start_voltage)
    print ('Starting voltage: ' + start_voltage)

    print('Waiting ' + str(sleep) + ' seconds...')
    time.sleep(sleep)

    unformatted_final_voltage = N3300A.query_ascii_values('MEASure:VOLTage?')
    final_voltage = str(round(float(unformatted_final_voltage[0]), rounding)) + 'V'
    worksheet.write(7, 1, final_voltage)
    print('Final voltage: ' + final_voltage + '\n')
    N3300A.write('CURRent 0')
    workbook.close()



N3300A.close()
rm.close()
